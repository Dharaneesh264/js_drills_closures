const cacheFunction = (callback) => {
    let cached = {};
    return (...args) => {
        res = {};
        arr = [];
       let n = args.length;
       for (let i = 0; i < n; i++) {
           if (res[args[i]] !== undefined) {
                res[args[i]] = 'Not a Callback function';
                arr.push(args[i], res[args[i]]);
            } else {
                res[args[i]] = callback();
                arr.push(args[i], res[args[i]]);
           }
       }
       return arr;
    }
}

module.exports = cacheFunction;