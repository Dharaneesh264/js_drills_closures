const counterFactory = require('../counterFactory');

let fun = counterFactory();

console.log(fun.increment());
console.log(fun.increment());
console.log(fun.decrement());
console.log(fun.increment());
console.log(fun.decrement());
console.log(fun.increment());