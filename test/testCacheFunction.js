const cacheFunction = require('../cacheFunction');

const cb = () => {
    return 'Callback function';
}

const cache = cacheFunction(cb);

console.log(cache(1, 2, 1, 4, 5));

console.log(cache(4, 5, 2, 4, 9));