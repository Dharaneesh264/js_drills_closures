const limitFunctionCallCount = require('../limitFunctionCallCount');

const cb = (counter) => {
    counter += 1;
    return counter;
}
const limitFunction = limitFunctionCallCount(cb, 2);
console.log(limitFunction());
console.log(limitFunction());
console.log(limitFunction());
const limitFunction1 = limitFunctionCallCount(cb, 1);
console.log(limitFunction1());
console.log(limitFunction1());
console.log(limitFunction1());