const counterFactory = () => {
    let counter = 0;
    return {
        increment: () => counter += 1,
        decrement: () => counter -= 1
    }
}

module.exports = counterFactory;