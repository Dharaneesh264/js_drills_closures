const limitFunctionCallCount = (callback, limit) => {
    let counter = 0;
    return () => {
        if (counter < limit) {
            counter = callback(counter);
            return counter;
        }
        return null;
    }
}

module.exports = limitFunctionCallCount;